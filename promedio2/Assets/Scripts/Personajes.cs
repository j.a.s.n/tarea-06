﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personajes : MonoBehaviour
{
    public GameObject player1;
    public GameObject player2;
    public GameObject player3;
    public GameObject player4;
    public bool players1;
    public bool players2;
    public bool players3;
    public bool players4;
    // Start is called before the first frame update
    void Start()
    {
        player1.SetActive(true);
        player2.SetActive(false);
        player3.SetActive(false);
        player4.SetActive(false);
        players1 = true;
        players2 = false;
        players3 = false;
        players4 = false;
    }

    // Update is called once per frame
    void Update()
    {
        if ((player1.activeSelf == true && player4.activeSelf == false) && Input.GetKeyDown(KeyCode.Space))
        {
            player1.SetActive(false);
            player2.SetActive(true);
            player3.SetActive(false);
            player4.SetActive(false);
            players1 = false;
            players2 = true;
            players3 = false;
            players4 = false;
        }
        else
        {

            if ((player2.activeSelf == true && player4.activeSelf == false && player1.activeSelf == false) && Input.GetKeyDown(KeyCode.Space))
            {

                player1.SetActive(false);
                player2.SetActive(false);
                player3.SetActive(true);
                player4.SetActive(false);
                players1 = false;
                players2 = false;
                players3 = true;
                players4 = false;




            }
            else
            {
                if ((player3.activeSelf == true && player1.activeSelf == false && player2.activeSelf == false) && Input.GetKeyDown(KeyCode.Space))
                {
                    player1.SetActive(false);
                    player2.SetActive(false);
                    player3.SetActive(false);
                    player4.SetActive(true);
                    players1 = false;
                    players2 = false;
                    players3 = false;
                    players4 = true;
                }
                else
                {
                    if ((player4.activeSelf == true && player1.activeSelf == false && player2.activeSelf == false) && Input.GetKeyDown(KeyCode.Space))
                    {
                        player1.SetActive(true);
                        player2.SetActive(false);
                        player3.SetActive(false);
                        player4.SetActive(false);
                        players1 = true;
                        players2 = false;
                        players3 = false;
                        players4 = false;

                    }



                }



            }
        }
    }           
}
