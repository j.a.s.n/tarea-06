﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Model : MonoBehaviour
{
    // Variables
    public string strClass;
    public int intClass;
    public GameObject player;
    public KeyCode up;
    public KeyCode right;
    public KeyCode left;
    public KeyCode select;
    public Material guerrero;
    public Material mago;
    public Material asesino;
}
