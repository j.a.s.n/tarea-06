﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour
{
    public Model mdl;
    public GameObject objPlay;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Renderer rend = mdl.player.GetComponent<Renderer>();
        if (Input.GetKeyDown(mdl.up))
        {
            rend.material = mdl.guerrero;
            mdl.strClass = "guerrero";
            mdl.intClass = 0;
        }

        if (Input.GetKeyDown(mdl.left))
        {
            rend.material = mdl.mago;
            mdl.strClass = "mago";
            mdl.intClass = 1;
        }

        if (Input.GetKeyDown(mdl.right))
        {
            rend.material = mdl.asesino;
            mdl.strClass = "asesino";
            mdl.intClass = 2;
        }

        if (Input.GetKeyDown(mdl.select))
        {
            Debug.Log("Has escogido ser un " + mdl.strClass);
        }
    }
}
