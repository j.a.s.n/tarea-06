﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flecha : MonoBehaviour
{
    public float pos = 0;
    public bool activo = true;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            pos += 1;

            Debug.Log(pos);

        }
        if (pos >= 2)
        {
            pos = -1;
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            pos -= 1;

            Debug.Log(pos);

        }
        if (pos <= -2)
        {
            pos = 1;
        }

        if(pos == 0)
        {
            if(activo == true)
            {
                GetComponent<Transform>().position = new Vector3(0, -0.09f, 0);
            }

            //GetComponent<Transform>().position = new Vector3(0,-0.09f,0);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameObject.FindGameObjectWithTag("cube1").GetComponent<Rigidbody2D>().gravityScale = 2;
                GameObject.FindGameObjectWithTag("cube3").GetComponent<Rigidbody2D>().gravityScale = 2;

                GameObject.FindGameObjectWithTag("tex1").GetComponent<Rigidbody2D>().gravityScale = 2;
                GameObject.FindGameObjectWithTag("tex3").GetComponent<Rigidbody2D>().gravityScale = 2;

                activo = false;
                transform.position = transform.position + new Vector3(0,0.1f,0);
            }
        }

        if (pos == 1)
        {
            if(activo == true)
            {
                GetComponent<Transform>().position = new Vector3(-4.90f, -0.09f, 0);
            }
         

            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameObject.FindGameObjectWithTag("cube2").GetComponent<Rigidbody2D>().gravityScale = 2;
                GameObject.FindGameObjectWithTag("cube3").GetComponent<Rigidbody2D>().gravityScale = 2;

                GameObject.FindGameObjectWithTag("tex2").GetComponent<Rigidbody2D>().gravityScale = 2;
                GameObject.FindGameObjectWithTag("tex3").GetComponent<Rigidbody2D>().gravityScale = 2;

                activo = false;
                transform.position = transform.position + new Vector3(0, 0.1f, 0);
            }
        }

        if (pos == -1)
        {
            if(activo == true)
            {
                GetComponent<Transform>().position = new Vector3(4.70f, -0.09f, 0);
            }
           

            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameObject.FindGameObjectWithTag("cube1").GetComponent<Rigidbody2D>().gravityScale = 2;
                GameObject.FindGameObjectWithTag("cube2").GetComponent<Rigidbody2D>().gravityScale = 2;

                GameObject.FindGameObjectWithTag("tex1").GetComponent<Rigidbody2D>().gravityScale = 2;
                GameObject.FindGameObjectWithTag("tex2").GetComponent<Rigidbody2D>().gravityScale = 2;

                activo = false;
                transform.position = transform.position + new Vector3(0, 0.1f, 0);
            }
        }
        }

    }

